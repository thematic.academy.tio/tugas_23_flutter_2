import 'package:flutter/material.dart';
import 'package:tugas_23/pages/PlayContent.dart';

// ignore: must_be_immutable
class Pageshome extends StatelessWidget {
  String value1;
  String value2;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
            appBar: AppBar(
              title: Text("Main Page"),
            ),
            body: Container(
              margin: EdgeInsets.all(20),
              child: ListView(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets.fromLTRB(0, 50, 0, 20),
                        child: Text(
                          "Team 1",
                          style: TextStyle(fontSize: 18),
                        ),
                      ),
                      TextField(
                        decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10))),
                        onChanged: (text1) {
                          value1 = text1;
                        },
                      ),
                      Container(
                          margin: EdgeInsets.fromLTRB(0, 20, 0, 20),
                          child:
                              Text("Team 2", style: TextStyle(fontSize: 18))),
                      TextField(
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.all(20),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10))),
                        onChanged: (text2) {
                          value2 = text2;
                        },
                      ),
                      Container(
                        margin: EdgeInsets.fromLTRB(0, 50, 0, 0),
                        width: 400,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20)),
                        child: RaisedButton(
                            padding: EdgeInsets.fromLTRB(50, 20, 50, 20),
                            color: Colors.blue,
                            child: Text(
                              "Mulai",
                              style:
                                  TextStyle(fontSize: 18, color: Colors.white),
                            ),
                            onPressed: () {
                              Navigator.push(context,
                                  MaterialPageRoute(builder: (contex) {
                                return PlayContent(
                                  value1: value1,
                                  value2: value2,
                                );
                              }));
                            }),
                      )
                    ],
                  )
                ],
              ),
            )));
  }
}
