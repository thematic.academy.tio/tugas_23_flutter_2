import 'package:flutter/material.dart';

// ignore: must_be_immutable
class PlayContent extends StatefulWidget {
  String value1;
  String value2;
  PlayContent({this.value1, this.value2});

  @override
  _PlayContentState createState() =>
      _PlayContentState(value1: value1, value2: value2);
}

class _PlayContentState extends State<PlayContent> {
  String value1;
  String value2;
  _PlayContentState({this.value1, this.value2});

  int number1 = 0;
  int number2 = 0;

  void handleClickPlus1() {
    setState(() {
      number1++;
    });
  }

  void handleClickPlus2() {
    setState(() {
      number2++;
    });
  }

  void handleClickMin1() {
    setState(() {
      number1--;
    });
  }

  void handleClickMin2() {
    setState(() {
      number2--;
    });
  }

  void handleReset() {
    setState(() {
      number1 = 0;
      number2 = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Score Count"),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Flexible(
                flex: 1,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Flexible(
                      flex: 1,
                      child: Container(
                        child: Text(
                          number1.toString(),
                          style: TextStyle(fontSize: 26, color: Colors.white),
                        ),
                        padding: EdgeInsets.fromLTRB(40, 15, 40, 15),
                        margin: EdgeInsets.fromLTRB(0, 20, 0, 20),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: Colors.grey,
                        ),
                      ),
                    ),
                    Flexible(
                      flex: 1,
                      child: Container(
                        child: Text(
                          number2.toString(),
                          style: TextStyle(fontSize: 26, color: Colors.white),
                        ),
                        padding: EdgeInsets.fromLTRB(40, 15, 40, 15),
                        margin: EdgeInsets.fromLTRB(0, 20, 0, 20),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: Colors.deepOrangeAccent,
                        ),
                      ),
                    ),
                  ],
                )),
            Flexible(
                flex: 1,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Flexible(
                        flex: 1,
                        child: Container(
                          child: Text(
                            value1.toString(),
                            maxLines: 2,
                            style: TextStyle(fontSize: 18, color: Colors.white),
                          ),
                          padding: EdgeInsets.all(20),
                          margin: EdgeInsets.all(20),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              color: Colors.blueGrey),
                        )),
                    Flexible(
                        flex: 1,
                        child: Container(
                          child: Text(value2.toString(),
                              maxLines: 2,
                              style:
                                  TextStyle(fontSize: 18, color: Colors.white)),
                          padding: EdgeInsets.all(20),
                          margin: EdgeInsets.all(20),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: Colors.deepOrange,
                          ),
                        ))
                  ],
                )),
            Flexible(
                flex: 1,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Flexible(
                      flex: 1,
                      child: Container(
                        margin: EdgeInsets.fromLTRB(0, 20, 0, 20),
                        child: SizedBox(
                          height: 50,
                          width: 100,
                          child: RaisedButton(
                            child: Text(
                              "+",
                              style:
                                  TextStyle(fontSize: 26, color: Colors.white),
                            ),
                            onPressed: handleClickPlus1,
                            color: Colors.blueGrey,
                          ),
                        ),
                      ),
                    ),
                    Flexible(
                      flex: 1,
                      child: SizedBox(
                        height: 50,
                        width: 100,
                        child: RaisedButton(
                          child: Text(
                            "+",
                            style: TextStyle(fontSize: 26, color: Colors.white),
                          ),
                          onPressed: handleClickPlus2,
                          color: Colors.deepOrange,
                        ),
                      ),
                    )
                  ],
                )),
            Flexible(
              flex: 1,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Flexible(
                    flex: 1,
                    child: Container(
                      margin: EdgeInsets.fromLTRB(0, 20, 0, 20),
                      child: SizedBox(
                        height: 50,
                        width: 100,
                        child: RaisedButton(
                          child: Text(
                            "-",
                            style: TextStyle(fontSize: 36, color: Colors.white),
                          ),
                          onPressed: handleClickMin1,
                          color: Colors.blueGrey,
                        ),
                      ),
                    ),
                  ),
                  Flexible(
                    flex: 1,
                    child: SizedBox(
                      height: 50,
                      width: 100,
                      child: RaisedButton(
                        child: Text(
                          "-",
                          style: TextStyle(fontSize: 36, color: Colors.white),
                        ),
                        onPressed: handleClickMin2,
                        color: Colors.deepOrange,
                      ),
                    ),
                  )
                ],
              ),
            ),
            Flexible(
              flex: 1,
              child: Container(
                margin: EdgeInsets.fromLTRB(0, 20, 0, 20),
                child: SizedBox(
                  height: 60,
                  width: 300,
                  child: RaisedButton(
                    child: Text(
                      "Reset",
                      style: TextStyle(fontSize: 18, color: Colors.white),
                    ),
                    onPressed: handleReset,
                    color: Colors.deepPurpleAccent,
                  ),
                ),
              ),
            ),
            Flexible(
              flex: 1,
              child: Container(
                margin: EdgeInsets.fromLTRB(0, 10, 0, 20),
                child: SizedBox(
                  height: 60,
                  width: 300,
                  child: RaisedButton(
                    child: Text(
                      "Kembali",
                      style: TextStyle(fontSize: 18, color: Colors.white),
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    color: Colors.red,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
